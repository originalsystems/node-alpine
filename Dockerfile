FROM node:11-alpine

RUN set -ex \
    && mkdir /home/node/.pm2 \
    && chown node:node /home/node/.pm2 \
    && yarn global add pm2

COPY docker-entrypoint.sh /docker-entrypoint.sh

EXPOSE 3000

ENTRYPOINT ["/docker-entrypoint.sh"]

CMD ["/usr/local/bin/pm2-runtime", "--json", "/var/www/html/events/index.js"]
