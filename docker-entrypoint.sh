#! /bin/sh

set -ex

umask 0002

exec "$@"
